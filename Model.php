<?php

require_once 'Helper.php';

abstract class Model
{
    /**
     * connection
     *
     * @var mixed
     */
    protected $connection;

    /**
     * table
     *
     * @var mixed
     */
    protected $table;

    /**
     * select
     *
     * @var array
     */
    protected $select = [];

    /**
     * whereClause
     *
     * @var array
     */
    protected $whereClause = [];

    /**
     * orderClause
     *
     * @var array
     */
    protected $orderClause = [];

    /**
     * groupClause
     *
     * @var array
     */
    protected $groupClause = [];

    /**
     * attributes
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * limit
     *
     * @var int
     */
    private $limit = 0;

    /**
     * __get attributes
     *
     * @param  mixed $name
     * @return void
     */
    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return null;
    }

    /**
     * connect to database
     *
     * @return void
     */
    private function connect()
    {
        $servername = config('database.host');
        $databasename = config('database.dbname');
        $username = config('database.username');
        $password = config('database.password');

        $this->connection = new PDO("mysql:host=$servername;dbname=$databasename", $username, $password);
    }

    /**
     * disconnect database
     *
     * @return void
     */
    private function disconnect()
    {
        $this->connection = null;
    }

    /**
     * Get the value of table
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Set the value of table
     *
     * @return  self
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }


    /**
     * query
     *
     * @return static
     */
    public static function query(): Model
    {
        return (new static)->newQuery();
    }


    /**
     * newQuery
     *
     * @return Model
     */
    private function newQuery(): Model
    {
        $this->setTable($this->getTable());
        return $this;
    }

    /**
     * check Fields in attributes
     *
     * @param  mixed $fieldName
     * @return bool
     */
    private function checkFields($fieldName)
    {
        return in_array($fieldName, $this->attributes);
    }

    /**
     * select
     *
     * @param  string $fields
     * @return $this
     */
    public function select(...$fields)
    {
        foreach ($fields as $field) {
            if ($this->checkFields($field)) {
                $this->select[] = $field;
            }
        }
        return $this;
    }

    /**
     * where
     *
     * @param  string $field
     * @param  string $operation
     * @param  string $value
     * @return $this
     */
    public function where(string $field, string $operation = '=', $value = null)
    {
        if ($this->checkFields($field)) {
            $this->whereClause[$field] = [$operation, $value];
        }
        return $this;
    }

    /**
     * orderBy
     *
     * @param  string $field
     * @param  string $order
     * @return $this
     */
    public function orderBy(string $field, string $order = 'asc')
    {
        if ($this->checkFields($field)) {
            $this->orderClause[$field] = $field . ' ' . $order;
        }
        return $this;
    }

    /**
     * orderByDesc
     *
     * @param  string $field
     * @return $this
     */
    public function orderByDesc(string $field)
    {
        if ($this->checkFields($field)) {
            $this->orderClause[$field] = $field . ' desc';
        }
        return $this;
    }

    /**
     * groupBy
     *
     * @param  string $field
     * @return $this
     */
    public function groupBy(string $field)
    {
        if ($this->checkFields($field) && in_array($field, $this->groupClause) === false) {
            $this->groupClause[] = $field;
        }
        return $this;
    }

    /**
     * set limit
     *
     * @param  int $limit
     * @return $this
     */
    public function limit(int $limit)
    {
        if ($limit > 0) {
            $this->limit = $limit;
        }

        return $this;
    }

    /**
     * get
     *
     * @return array
     * @throws Exception
     */
    public function get()
    {
        $query = 'SELECT';

        if (count($this->select)) {
            $query .= ' ' . implode(',', $this->select);
        } else {
            $query .= ' * ';
        }

        if (!$this->getTable()) {
            throw new Exception('Underfined table');
        }

        $query .= ' from ' . $this->getTable();

        if (count($this->whereClause)) {
            $query .= ' where 1';
            foreach ($this->whereClause as $key => $val) {
                $query .= " and {$key} {$val[0]} '{$val[1]}'";
            }
        }

        if (count($this->groupClause)) {
            $query .= " group by " . implode(' ,', $this->groupClause);
        }

        if (count($this->orderClause)) {
            $query .= " order by " . implode(' ,', $this->orderClause);
        }

        if ($this->limit) {
            $query .= " limit " . $this->limit;
        }

        $query .=  ";";

        $this->connect();

        $re = $this->connection->query($query)->fetchAll(PDO::FETCH_ASSOC);

        $this->disconnect();

        // return $re;

        $result = [];

        foreach ($re as $res) {
            $opject = new static;
            $opject->data = $res;
            $result[] = $opject;
        }

        return $result;
    }

    /**
     * getFirst
     *
     * @return object
     * @throws Exception
     */
    public function getFirst()
    {
        $this->limit = 1;
        return $this->get()[0];
    }

    /**
     * getAll
     *
     * @return array
     * @throws Exception
     */
    public function getAll()
    {
        $clone = new static();
        return $clone->get();
    }
}