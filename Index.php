<?php

require_once 'IT.php';
require_once 'ConvertSale.php';

$its = IT::query()->where('bonus', '>', 0)->orderBy('bonus', 'desc')->get();
$cvss = ConvertSale::query()->select('name', 'salary', 'kpi')->where('kpi', '>', 20)->orderBy('kpi', 'desc')->get();

for ($i = 0; $i < count($cvss); $i++) {
    $cvss[$i]->data['salary_received'] = $cvss[$i]->getSalary();
}
// print_r($its);

function sortBySalary($a, $b)
{
    return $b->data['salary_received'] <=> $a->data['salary_received'];
}

usort($cvss, 'sortBySalary');

print_r($cvss);