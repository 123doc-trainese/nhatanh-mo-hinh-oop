<?php

if (!function_exists('config')) {
    function config(string $path)
    {
        $configs = require __DIR__ . '/config.php';
        $param = explode('.', $path);

        foreach ($param as $val) {
            if (isset($configs[$val])) {
                $configs = $configs[$val];
            } else {
                return null;
            }
        }
        return $configs;
    }
}