<?php

require_once 'Model.php';
require_once 'Staff.php';

class IT extends Model implements Staff
{
    protected $table = 'it';

    protected $attributes = [
        'id',
        'name',
        'birthday',
        'salary',
        'bonus'
    ];

    public function getSalary()
    {
        return ($this->salary ?: 0) + ($this->bonus ?: 0);
    }
}