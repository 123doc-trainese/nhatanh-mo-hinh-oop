# Mô hình OOP
## Yêu cầu

- Hiểu được OOP là gì?
- Nắm rõ 4 tính chất của OOP: đóng gói, đa hình, trừu tượng, kế thừa
 - Các thuộc tính private, public, protected, static
 - Extend, implement
- Phân biệt sự khác nhau giữa interface và abstract class

### Lý thuyết:

- https://xuanthulab.net/lap-trinh-huong-doi-tuong-oop-voi-php.html

### Thực hành
Xây dựng Repo có cấu trúc thư mục như sau:

- **Model.php**: Là base class kết nối và thực thi SQL
 - Các thuộc tính: `$table`, `$select`, `$whereClause`, `$orderClause`, `$groupClause`, `$connection`
 - Các phương thức: `connect()`, `query()`, `getFirst()`, `getAll()`, `get()`, `where()`, `orderBy()`, `groupBy()`
- **Staff.php**: Đối tượng nhân viên
 - Các phương thức: `getSalary()`
- **ConvertSale.php**: Đối tượng nhân viên khối ConvertSale
 - Công thức tính lương:
    - KPI: `100 link`
    - Lương cứng nếu đủ KPI: `6.000.000đ`
    - Thiếu KPI: `1 - 20` phạt `10.000đ/link`, `21 - 100` phạt `15.000d/link`
    - Thừa KPI: `15.000đ/link`
- **IT.php**: Đối tượng nhân viên khối IT
 - Công thức tính lương:
    - Lương cứng: `8.000.000đ`
    - Thưởng: tùy tháng
- **index.php**: Thực thi lấy ra lương của 5 nhân viên trong tháng
### Kiến thức tìm hiểu được
- OOP lập trình hướng đối tượng là một phương pháp lập trình dựa trên khái niệm về lớp và đối tượng. OOP tập trung vào các đối tượng thao tác hơn là logic để thao tác chúng, giúp code dễ quản lý, tái sử dụng được và dễ bảo trì.

- tính đóng gói: giúp bảo mật dữ liệu trong đối tượng. 3 loại 
    - private : chỉ được truy cập bên trong class khai báo
    - public : có thể truy cập ở bất cứ đâu
    - protected : chỉ có thể truy cập thông qua kế thừa

- tính kế thừa: cho 1 class có thể kế thừa thuộc tính, phương thức từ lớp khác
    - abstract class : là 1 lớp để kế thừa, đơn kế thừa. dùng khi các chức năng trong abstract chắc chắn dùng trong các lớp con kế thừa

    - interface : là 1 khuôn mẫu. class dùng implement để kế thừa từ 1 interface. khi implement 1 interface phải khai báo và định nghĩa tất cả các hàm trong interface. dùng khi muốn tạo 1 khung các chức năng
- tính trừu tượng : Đây là khả năng của chương trình bỏ qua hay không chú ý đến một số khía cạnh của thông tin mà nó đang trực tiếp làm việc lên, nghĩa là nó có khả năng tập trung vào những cốt lõi cần thiết.

- tính đa hình : Tính đa hình là một hành động có thể được thực hiện bằng nhiều cách khác nhau. 
### Thực hành
- xây dựng Repo
- kết nối csdl
- lấy danh sách nhân viên it, nhân viên sale
- tính lương nhận được cho nhân viên sale và nhân viên it
### Final
```
its = IT::query()->where('bonus', '>', 0)->orderBy('bonus', 'desc')->get();
$cvss = ConvertSale::query()->select('name', 'salary', 'kpi')->where('kpi', '>', 20)->orderBy('kpi', 'desc')->get();

for ($i = 0; $i < count($cvss); $i++) {
    $cvss[$i]->data['salary_received'] = $cvss[$i]->getSalary();
}

function sortBySalary($a, $b)
{
    return $b->data['salary_received'] <=> $a->data['salary_received'];
}

usort($cvss, 'sortBySalary');

print_r($cvss);

//output
// danh sách nhân viên đã sắp xếp theo lương
Array
(
    [0] => ConvertSale Object
        (
            [connection:protected] => 
            [table:protected] => cvs
            [select:protected] => Array
                (
                )

            [whereClause:protected] => Array
                (
                )

            [orderClause:protected] => Array
                (
                )

            [groupClause:protected] => Array
                (
                )

            [attributes:protected] => Array
                (
                    [0] => id
                    [1] => name
                    [2] => birthday
                    [3] => salary
                    [4] => kpi
                )

            [limit:Model:private] => 0
            [data] => Array
                (
                    [name] => name 4
                    [salary] => 6000000
                    [kpi] => 130
                    [salary_received] => 6450000
                )

        )

    [1] => ConvertSale Object
        (
            [connection:protected] => 
            [table:protected] => cvs
            [select:protected] => Array
                (
                )

            [whereClause:protected] => Array
                (
                )

            [orderClause:protected] => Array
                (
                )

            [groupClause:protected] => Array
                (
                )

            [attributes:protected] => Array
                (
                    [0] => id
                    [1] => name
                    [2] => birthday
                    [3] => salary
                    [4] => kpi
                )

            [limit:Model:private] => 0
            [data] => Array
                (
                    [name] => name 3
                    [salary] => 6000000
                    [kpi] => 120
                    [salary_received] => 6300000
                )

        )

    [2] => ConvertSale Object
        (
            [connection:protected] => 
            [table:protected] => cvs
            [select:protected] => Array
                (
                )

            [whereClause:protected] => Array
                (
                )

            [orderClause:protected] => Array
                (
                )

            [groupClause:protected] => Array
                (
                )

            [attributes:protected] => Array
                (
                    [0] => id
                    [1] => name
                    [2] => birthday
                    [3] => salary
                    [4] => kpi
                )

            [limit:Model:private] => 0
            [data] => Array
                (
                    [name] => name 1
                    [salary] => 6000000
                    [kpi] => 80
                    [salary_received] => 5800000
                )

        )

    [3] => ConvertSale Object
        (
            [connection:protected] => 
            [table:protected] => cvs
            [select:protected] => Array
                (
                )

            [whereClause:protected] => Array
                (
                )

            [orderClause:protected] => Array
                (
                )

            [groupClause:protected] => Array
                (
                )

            [attributes:protected] => Array
                (
                    [0] => id
                    [1] => name
                    [2] => birthday
                    [3] => salary
                    [4] => kpi
                )

            [limit:Model:private] => 0
            [data] => Array
                (
                    [name] => name 2
                    [salary] => 6000000
                    [kpi] => 50
                    [salary_received] => 5250000
                )

        )

)
```